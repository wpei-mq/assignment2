import java.awt.*;

import bos.RelativeMove;

import java.util.Optional;

public abstract class Character implements Npc {
    protected Npc decoratedNpc;

    Optional<Image> display;
    Cell location;
    Behaviour behaviour;
    protected int movement;
    protected int movesLeft;


    public Character(Cell location, Behaviour behaviour){
        this.location = location;
        this.display = Optional.empty();
        this.behaviour = behaviour;
        this.movement = 1;
        this.movesLeft = 1;
    }

    public Character(Npc decoratedNpc){
        this.decoratedNpc = decoratedNpc;
        this.behaviour = decoratedNpc.getBehaviour();
        this.location = decoratedNpc.getLocationOf();
        this.movement = decoratedNpc.getMovement();
        this.movesLeft = decoratedNpc.getMovesLeft();
    }

    public void paint(Graphics g){
        if (decoratedNpc != null) {
            decoratedNpc.paint(g);
        }
        if(display.isPresent()) {
            g.drawImage(display.get(), location.x+2, location.y+2, 31, 31, null, null);
        }
    }

    @Override
    public void setMovement(int movement) {
        this.movement = movement;
    }

    @Override
    public int getMovement() {
        return this.movement;
    }

    @Override
    public void setMovesLeft(int movesLeft) {
        this.movesLeft = movesLeft;
    }

    @Override
    public int getMovesLeft() {
        return this.movesLeft;
    }

    public void reduceMoves() {
        movesLeft = movesLeft - 1;
    }

    public void resetMoves() {
        movesLeft = movement;
    }

    @Override
    public void setBehaviour(Behaviour behaviour) {
        this.behaviour = behaviour;
    }

    @Override
    public Behaviour getBehaviour() {
        return this.behaviour;
    }

    @Override
    public RelativeMove aiMove(Stage stage) {
        if (decoratedNpc != null) {
            return decoratedNpc.aiMove(stage);
        } else {
            return behaviour.chooseMove(stage, this);
        }
    }

    @Override
    public Cell getLocationOf() {
        return this.location;
    }

    @Override
    public void setLocationOf(Cell location) {
        this.location = location;
    }
}