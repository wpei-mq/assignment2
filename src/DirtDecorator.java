import java.awt.*;
import java.util.Random;

public class DirtDecorator extends Character {
    private Random random = new Random();
    private int r;
    private int s;

    public DirtDecorator(Npc decoratedNpc) {
        super(decoratedNpc);
        r = random.nextInt(15);
        s = random.nextInt(15);
    }

    @Override
    public void paint(Graphics g) {
        decoratedNpc.paint(g);
        g.setColor(new Color(102, 51, 0));
        g.fillOval(decoratedNpc.getLocationOf().x + decoratedNpc.getLocationOf().width/4 + r, decoratedNpc.getLocationOf().y+ decoratedNpc.getLocationOf().height/4 + s,
                decoratedNpc.getLocationOf().width/6, decoratedNpc.getLocationOf().height/6);
    }

    @Override
    public String getCharType() {
        return decoratedNpc.getCharType();
    }
}
