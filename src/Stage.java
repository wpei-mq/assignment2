import java.awt.*;
import java.util.*;
import java.time.*;
import java.util.function.BiConsumer;

import bos.GameBoard;

public class Stage {
    protected static Grid grid;
    protected Character sheep;
    protected Character shepherd;
    protected Character wolf;
    protected Player player;
    private ArrayList<BiConsumer<java.lang.Character, GameBoard<Cell>>> observers;
    private ArrayList<Character> characters = new ArrayList<Character>();

    private Instant timeOfLastMove = Instant.now();
    private Boolean sheepStuck = false;

    public Stage() {
        grid = new Grid(10, 10);
        characters.add(new Shepherd(grid.getCell(0, 0), new StandStill()));
        characters.add(new Sheep(grid.getCell(19, 0), new MoveTowards(characters.get(0))));
        characters.add(new Wolf(grid.getCell(19, 19), new MoveTowards(characters.get(1))));
        observers = new ArrayList();

        player = new Player(grid.getRandomCell());
        observers.add(player::notify);
    }

    public void update() {
        if (!player.inMove()) {
            if (characters.get(1).getLocationOf() == characters.get(0).getLocationOf()) {
                System.out.println("The sheep is safe :)");
                System.exit(0);
            } else if (characters.get(1).getLocationOf() == characters.get(2).getLocationOf()) {
                System.out.println("The sheep is dead :(");
                System.exit(1);
            } else {
                sheepStuck = false;
                characters.forEach(c -> {
                    if (c.getCharType() == "Sheep" && c.getLocationOf().x == c.getLocationOf().y) {
                        c.setBehaviour(new StandStill());
                        sheepStuck = true;
                    }
                    if (c.getCharType() == "Shepherd" && sheepStuck == true) {
                        c.setBehaviour(new MoveTowards(characters.get(1)));
                    }

                    if (c.getMovesLeft() > 0) {
                        c.aiMove(this).perform();
                        c.reduceMoves();
                    }

                    if (grid.getCellType(c.getLocationOf()) == "Grass" && c.getMovesLeft() >= 0) {
                        int i = characters.indexOf(c);
                        Character decoratedCharacter;
                        decoratedCharacter = new GrassDecorator(c);
                        decoratedCharacter.reduceMoves();
                        characters.set(i, decoratedCharacter);
                    } else if (grid.getCellType(c.getLocationOf()) == "Dirt" && c.getMovesLeft() >= 0) {
                        int i = characters.indexOf(c);
                        Character decoratedCharacter;
                        decoratedCharacter = new DirtDecorator(c);
                        decoratedCharacter.reduceMoves();
                        characters.set(i, decoratedCharacter);
                    }
                });

                if (characters.get(0).getMovesLeft() <= 0
                        && characters.get(1).getMovesLeft() <= 0
                        && characters.get(2).getMovesLeft() <= 0) {
                    player.startMove();
                    timeOfLastMove = Instant.now();
                    characters.forEach(c -> c.resetMoves());
                }
            }
        }
    }

    public void paint(Graphics g, Point mouseLocation) {
        grid.paint(g, mouseLocation);
        characters.forEach(c -> c.paint(g));
        player.paint(g);
    }

    public void notifyAll(char c){
        for(BiConsumer bc : observers) {
            bc.accept(new java.lang.Character(c), grid);
        }
    }
}