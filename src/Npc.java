import bos.GamePiece;
import bos.RelativeMove;

import java.awt.*;

public interface Npc extends GamePiece<Cell> {
    void paint(Graphics g);
    void setBehaviour(Behaviour behaviour);
    Behaviour getBehaviour();
    void setMovement(int movement);
    int getMovement();
    void setMovesLeft(int movesLeft);
    int getMovesLeft();
    RelativeMove aiMove(Stage stage);
    String getCharType();
}
